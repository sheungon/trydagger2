package com.sheungon.trydagger2;

import android.app.Application;

import com.sheungon.trydagger2.sdk.api.DaggerServerApiComponent;
import com.sheungon.trydagger2.sdk.api.ServerApiComponent;
import com.sheungon.trydagger2.sdk.api.ServerConfigModule;

/**
 * @author John
 */

public class DaggerApplication extends Application {

    ServerApiComponent mServerApiComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        ServerConfigModule serverConfigModule = new ServerConfigModule();
        serverConfigModule.setMobileApiBaseUrl("https://www.google.com");
        serverConfigModule.setOAuthApiBaseUrl("https://hk.yahoo.com");

        mServerApiComponent = DaggerServerApiComponent.builder()
                .serverConfigModule(serverConfigModule)
                .build();
    }

    public ServerApiComponent getComponent() {
        return mServerApiComponent;
    }
}
