package com.sheungon.trydagger2.sdk.api.mobile;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by John on 2016-07-07.
 */

@Qualifier
@Retention(RetentionPolicy.SOURCE)
public @interface MobileApiBaseUrl {
}
