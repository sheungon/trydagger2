package com.sheungon.trydagger2.sdk.api.oauth;

import android.support.annotation.NonNull;

import com.sheungon.trydagger2.sdk.annotations.PerConnection;
import com.sheungon.trydagger2.sdk.api.RetrofitBuilderModule;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sheungon on 6/7/2016.
 */

@Module
public class OAuthApiModule {


    @NonNull
    @Provides
    @PerConnection
    public OAuthService getOAuthService(@OAuthApiBaseUrl @NonNull String baseUrl, RetrofitBuilderModule retrofitBuilderModule) {
        return retrofitBuilderModule
                .createRetrofit(baseUrl)
                .create(OAuthService.class);
    }
}
