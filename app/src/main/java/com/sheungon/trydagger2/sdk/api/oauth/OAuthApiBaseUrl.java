package com.sheungon.trydagger2.sdk.api.oauth;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by sheungon on 6/7/2016.
 */
@Qualifier
@Retention(RetentionPolicy.SOURCE)
public @interface OAuthApiBaseUrl {
}
