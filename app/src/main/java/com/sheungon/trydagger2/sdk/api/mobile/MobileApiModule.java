package com.sheungon.trydagger2.sdk.api.mobile;

import android.support.annotation.NonNull;

import com.sheungon.trydagger2.sdk.annotations.PerConnection;
import com.sheungon.trydagger2.sdk.api.RetrofitBuilderModule;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sheungon on 6/7/2016.
 */

@Module
public class MobileApiModule {

    @NonNull
    @Provides
    @PerConnection
    public MobileApiService getMobileApiService(@MobileApiBaseUrl String baseUrl, RetrofitBuilderModule retrofitBuilderModule) {
        return retrofitBuilderModule
                .createRetrofit(baseUrl)
                .create(MobileApiService.class);
    }
}
