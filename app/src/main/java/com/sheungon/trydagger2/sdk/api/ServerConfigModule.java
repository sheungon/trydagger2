package com.sheungon.trydagger2.sdk.api;

import com.sheungon.trydagger2.sdk.annotations.PerConnection;
import com.sheungon.trydagger2.sdk.api.mobile.MobileApiBaseUrl;
import com.sheungon.trydagger2.sdk.api.oauth.OAuthApiBaseUrl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by John on 2016-07-13.
 */

@Module
public class ServerConfigModule {

    private String mMobileApiBaseUrl;
    private String mOAuthApiBaseUrl;


    @Provides
    @MobileApiBaseUrl
    @PerConnection
    public String getMobileApiBaseUrl() {
        return mMobileApiBaseUrl;
    }

    public void setMobileApiBaseUrl(String mobileApiBaseUrl) {
        mMobileApiBaseUrl = mobileApiBaseUrl;
    }

    @Provides
    @OAuthApiBaseUrl
    @PerConnection
    public String getOAuthApiBaseUrl() {
        return mOAuthApiBaseUrl;
    }

    public void setOAuthApiBaseUrl(String OAuthApiBaseUrl) {
        mOAuthApiBaseUrl = OAuthApiBaseUrl;
    }
}
