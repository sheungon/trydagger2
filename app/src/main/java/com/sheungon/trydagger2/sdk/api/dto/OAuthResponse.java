package com.sheungon.trydagger2.sdk.api.dto;

/**
 * @author John
 */

public class OAuthResponse {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
