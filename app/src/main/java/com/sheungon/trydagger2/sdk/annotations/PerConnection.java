package com.sheungon.trydagger2.sdk.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by John on 2016-07-07.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerConnection {
}
