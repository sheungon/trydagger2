package com.sheungon.trydagger2.sdk.api.oauth;

import com.sheungon.trydagger2.sdk.api.dto.OAuthResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by sheungon on 6/7/2016.
 */
public interface OAuthService {

    String APP_ID = "appId";

    @GET("oauth/{" + APP_ID + "}")
    Call<OAuthResponse> getToken(@Path(APP_ID) String appId);
}
