package com.sheungon.trydagger2.sdk.api;

import com.sheungon.trydagger2.MainActivity;
import com.sheungon.trydagger2.sdk.annotations.PerConnection;
import com.sheungon.trydagger2.sdk.api.mobile.MobileApiModule;
import com.sheungon.trydagger2.sdk.api.oauth.OAuthApiModule;

import dagger.Component;

/**
 * @author John
 */

@PerConnection
@Component(modules =
        {NetModule.class,
                RetrofitBuilderModule.class,
                ServerConfigModule.class,
                MobileApiModule.class,
                OAuthApiModule.class})
public interface ServerApiComponent {

    void inject(MainActivity activity);
}
