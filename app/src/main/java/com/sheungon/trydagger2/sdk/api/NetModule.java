package com.sheungon.trydagger2.sdk.api;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sheungon.trydagger2.sdk.annotations.PerConnection;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

/**
 * Created by John on 2016-07-07.
 */

@Module
public class NetModule {


    @Inject
    public NetModule() {
    }

    @NonNull
    @Provides
    @PerConnection
    Gson createGson() {
        return new GsonBuilder().create();
    }

    @NonNull
    @Provides
    @PerConnection
    OkHttpClient createOkHttpClient() {
        return new OkHttpClient();
    }
}
