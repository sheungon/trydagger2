package com.sheungon.trydagger2.sdk.api;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.sheungon.trydagger2.sdk.annotations.PerConnection;
import com.sheungon.trydagger2.sdk.api.mobile.MobileApiBaseUrl;
import com.sheungon.trydagger2.sdk.api.oauth.OAuthApiBaseUrl;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author John
 */
@Module
public class RetrofitBuilderModule {

    @Inject
    Gson mGson;
    @Inject
    OkHttpClient mOkHttpClient;


    @Inject
    public RetrofitBuilderModule() {
    }

    @NonNull
    public Retrofit createRetrofit(@NonNull String baseUrl) {

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(mGson))
                .client(mOkHttpClient)
                .build();
    }
}
