package com.sheungon.trydagger2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.sheungon.trydagger2.sdk.api.mobile.MobileApiService;
import com.sheungon.trydagger2.sdk.api.oauth.OAuthService;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.msg) TextView mMsg;

    @Inject
    OAuthService mOAuthService;
    @Inject
    MobileApiService mMobileApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        ((DaggerApplication)getApplication()).getComponent()
                .inject(this);

        Log.d("MainActivity", "mOAuthService : " + mOAuthService);
        Log.d("MainActivity", "mMobileApiService : " + mMobileApiService);

        mMsg.setText("mOAuthService : " + mOAuthService);
        mMsg.append("\n");
        mMsg.append("mMobileApiService : " + mMobileApiService);

        ((DaggerApplication)getApplication()).getComponent()
                .inject(this);

        Log.d("MainActivity", "mOAuthService : " + mOAuthService);
        Log.d("MainActivity", "mMobileApiService : " + mMobileApiService);
    }
}
